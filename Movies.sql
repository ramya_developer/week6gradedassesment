create database Movies;
use Movies;
create table Movies_Coming(ID int,
Title varchar(20),
Year int,
Category varchar(20));


create table Movies_inTheatres
 (
  ID                int,
  Title             varchar(20),
  Year              int,
  Category          varchar(20)
);

create table Top_RatedIndia
 (
  ID                int,
  Title             varchar(20),
  Year              int,
  Category          varchar(20)
);


create table Top_RatedMovies
 (
  ID                int,
  Title             varchar(20),
  Year              int,
  Category          varchar(20)
);


insert into Movies_Coming values(1,'RRR',2022,'Movies Coming');
insert into Movies_Coming values(2,'Radheshyam',2022,'Movies Coming');
insert into Movies_coming values(3,'valimai',2022,'Movies Comming');

insert into Movies_inTheatres values(1,'Pushpa',2021,'Movies in Theatres');
insert into Movies_inTheatres values(2,'Marvel',2021,'Movies in Theatres');
insert into Movies_inTheatres values(3,'Kalank',2021,'Movies in Theatres');


insert into Top_RatedIndia values(1,'TareZameenpar',2007,'Top Rated India');
insert into Top_RatedIndia values(2,'Kesari',2020,'Top Rated India');
insert into Top_RatedIndia values(3,'Drishyam2',2021,'Top Rated India');

insert into Top_RatedMovies values(1,'Race3',2018,'Top Rated Movies');
insert into Top_RatedMovies values(2,'Jaibhim',2021,'Top Rated Movies');
insert into Top_RatedMovies values(3,'Sooryavanshi',2021,'Top Rated Movies');


select*from Movies_Coming;
select*from Top_RatedIndia;
select*from Top_RatedMovies;
select*from Movies_inTheatres;